# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DataLoadDialog.ui'
#
# Created: Wed Aug 20 11:37:55 2014
#      by: PyQt4 UI code generator 4.9.6
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_DataLoadDialog(object):
    def setupUi(self, DataLoadDialog):
        DataLoadDialog.setObjectName(_fromUtf8("DataLoadDialog"))
        DataLoadDialog.resize(523, 403)
        DataLoadDialog.setModal(True)
        self.verticalLayout = QtGui.QVBoxLayout(DataLoadDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(DataLoadDialog)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.ledSettingsFileName = QtGui.QLineEdit(DataLoadDialog)
        self.ledSettingsFileName.setObjectName(_fromUtf8("ledSettingsFileName"))
        self.horizontalLayout.addWidget(self.ledSettingsFileName)
        self.tbOpenSelectSettingsFile = QtGui.QToolButton(DataLoadDialog)
        self.tbOpenSelectSettingsFile.setObjectName(_fromUtf8("tbOpenSelectSettingsFile"))
        self.horizontalLayout.addWidget(self.tbOpenSelectSettingsFile)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_2 = QtGui.QLabel(DataLoadDialog)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_3.addWidget(self.label_2)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.chkCheckScanValue = QtGui.QCheckBox(DataLoadDialog)
        self.chkCheckScanValue.setChecked(True)
        self.chkCheckScanValue.setObjectName(_fromUtf8("chkCheckScanValue"))
        self.horizontalLayout_3.addWidget(self.chkCheckScanValue)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.listFiles = QtGui.QListWidget(DataLoadDialog)
        self.listFiles.setSelectionRectVisible(True)
        self.listFiles.setObjectName(_fromUtf8("listFiles"))
        self.verticalLayout.addWidget(self.listFiles)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.btnLoadFiles = QtGui.QPushButton(DataLoadDialog)
        self.btnLoadFiles.setObjectName(_fromUtf8("btnLoadFiles"))
        self.horizontalLayout_2.addWidget(self.btnLoadFiles)
        self.btnClearList = QtGui.QPushButton(DataLoadDialog)
        self.btnClearList.setObjectName(_fromUtf8("btnClearList"))
        self.horizontalLayout_2.addWidget(self.btnClearList)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.line = QtGui.QFrame(DataLoadDialog)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.buttonBox = QtGui.QDialogButtonBox(DataLoadDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Open)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(DataLoadDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), DataLoadDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), DataLoadDialog.reject)
        QtCore.QObject.connect(self.tbOpenSelectSettingsFile, QtCore.SIGNAL(_fromUtf8("clicked()")), DataLoadDialog.selectSetupFile)
        QtCore.QObject.connect(self.btnLoadFiles, QtCore.SIGNAL(_fromUtf8("clicked()")), DataLoadDialog.selectDataFiles)
        QtCore.QObject.connect(self.btnClearList, QtCore.SIGNAL(_fromUtf8("clicked()")), self.listFiles.clear)
        QtCore.QMetaObject.connectSlotsByName(DataLoadDialog)

    def retranslateUi(self, DataLoadDialog):
        DataLoadDialog.setWindowTitle(_translate("DataLoadDialog", "Dialog", None))
        self.label.setText(_translate("DataLoadDialog", "<html><head/><body><p><span style=\" font-size:10pt; text-decoration: underline;\">Файл с настройками:</span></p></body></html>", None))
        self.tbOpenSelectSettingsFile.setText(_translate("DataLoadDialog", "...", None))
        self.label_2.setText(_translate("DataLoadDialog", "<html><head/><body><p><span style=\" font-size:10pt; text-decoration: underline;\">Файлы для загрузки:</span></p></body></html>", None))
        self.chkCheckScanValue.setText(_translate("DataLoadDialog", "Проверять Scan-Value", None))
        self.btnLoadFiles.setText(_translate("DataLoadDialog", "Выбрать файлы...", None))
        self.btnClearList.setText(_translate("DataLoadDialog", "Очистить список", None))

