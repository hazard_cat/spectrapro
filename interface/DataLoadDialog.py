# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
from interface._DataLoadDialog import Ui_DataLoadDialog


class DataLoadDialog(QtGui.QDialog, Ui_DataLoadDialog):
    def __init__(self,parent=None):
        QtGui.QDialog.__init__(self,parent)
        self.setupUi(self)
        self.setfile = ""
        self.datafiles = []

    def selectSetupFile(self):
        self.setfile = QtGui.QFileDialog.getOpenFileName(
                            self,
                            "Select configuration file",
                            "",
                            "Setup file (*.set)")
        self.setfile = str(self.setfile)

        self.ledSettingsFileName.setText(self.setfile)
        
        
    def selectDataFiles(self):
        self.datafiles = QtGui.QFileDialog.getOpenFileNames(
                            self,
                            "Select data files",
                            "",
                            "Image files (*.im7 *imx)")
        
        self.listFiles.clear()
        if len(self.datafiles) !=0:
            for item in self.datafiles:
                self.listFiles.addItem(item)

    @staticmethod
    def getFiles(parent=None):
        dialog = DataLoadDialog(parent)
        result = dialog.exec_()
        setfile = dialog.setfile
        scanvalue = dialog.chkCheckScanValue.isChecked()
        datafiles = dialog.datafiles
        return (setfile, datafiles, scanvalue, result==QtGui.QDialog.Accepted)

