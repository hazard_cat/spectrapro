# -*- coding: utf-8 -*-
"""
Created on Mon Jul 21 15:13:45 2014

@author: Администратор
"""

from libim7 import libim7
import pylab as plt
import numpy as np
import glob
import matplotlib

font = {'family' : 'Times New Roman',
        'weight' : 'normal',
        'size'   : 14}

matplotlib.rc('font', **font)

#PATH = u"g:/#DATA/Ильин/2009_11_17/333/333_40fs_200mkm_1khz"
PATH = "g:/#DATA/Ильин/2009_11_17/issledovanie vozduh/50_2_1mj_1khz/337_100mk_200ps_1000ms/337_51_61/"

std_thres = 0.10
I_thres = 0.15

f1 = plt.figure(1)


d = {
    'interpolation':'nearest',
    'origin':'lower',
    'aspect':'auto'}

files = glob.glob(glob.os.path.join(PATH,'*.imx'))

for ifile in files:

    buf, att = libim7.readim7(ifile)
    att=att.as_dict()
    frame = buf.get_frame(0)
    
    vert = np.mean(frame, axis=1)
    vert = vert / vert.max()
    std = vert.std()
    if std>std_thres:
        idx = np.where(vert>I_thres)[0]
        ia = idx[0]
        ib = idx[-1]
        spectra = np.mean(frame[ia:ib,:], axis=0)
        extract = (ia,ib)
    
        dx = {'extent':(buf.x[0],buf.x[-1],buf.y[0],buf.y[-1])}
        dx.update(d)
        f1.clear()
        ax0 = f1.add_subplot(211)
        ax1 = f1.add_subplot(212)
        
        im = ax0.imshow(frame, **dx)
        if 'Scan-Value' in list(att.keys()):
            ax0.set_title('Scan-Value: %s'%att['Scan-Value'])
        ax0.set_xlabel(buf.scaleX.description.replace('\n',' ')+buf.scaleX.unit)
        ax0.set_ylabel(buf.scaleY.description.replace('\n',' ')+buf.scaleY.unit)
        cb = plt.colorbar(im, ax = ax0)
        cb.set_label(buf.scaleI.description.replace('\n',' ')+buf.scaleI.unit)

        ax1.plot(buf.x, spectra)
        ax1.set_xlabel(buf.scaleX.description.replace('\n',' ')+buf.scaleX.unit)
        ax1.set_title('rows from %s to %s ' % extract)
        plt.savefig(ifile.replace('.imx','.jpg'))

plt.show()