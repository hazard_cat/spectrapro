# -*- coding: utf-8 -*-
"""
Created on Fri Aug 15 00:19:08 2014

@author: Константин
"""

import numpy as np
import glob
import pylab as plt
from scipy.optimize import leastsq, fmin_l_bfgs_b

#a,b,A,mu,sigma = x

Gauss = lambda p, x: np.exp(-np.log(2)*((x-p[0])**2)/p[1]**2)
Linear= lambda p, x: p[0]*x/x+p[1]
Logentz = lambda p,x: 1/(1+((x-p[0])/p[1])**2)

Voigt = lambda p, x: p[0]*(p[1]*Logentz(p[2:4],x)+(1-p[1])*Gauss(p[2:4],x))+Linear(p[4:6],x)

Comb  = lambda p, x: Gauss(p,x)+Linear(p,x)
Error = lambda p, x, y: Voigt(p,x) - y


def substract_bgr(x,y,niter):
    y0=y    
    x0 = x
    for i in range(niter):
        A = np.column_stack((x, np.ones(len(x)),))
        a,b=np.linalg.lstsq(A, y)[0]
        y1 = a*x+b
        dy = np.abs(y1-y) 
        std = np.std(dy)
        idx = dy < std
        y=y[idx]
        x=x[idx]
    return (a*x0+b)
    

def fit_data(p0, x, y):
    
    p1, success = leastsq(Error, p0, args=(x,y))
    return Voigt(p1, x)
    

PATH = 'd:/Soft/WinPython/work/Ilyin/spectra/777/777_1'
MASK = '*.npz'

files = glob.glob(glob.os.path.join(PATH,MASK))
N_files = len(files)

p0 = [1.0, 0.5, 740, 10, 0.1, 1 ]

for ifile in files[1:10]:
    bname = glob.os.path.basename(ifile)
    delay = float(bname.split('.')[0].split('=')[1].replace('_','.'))
    print(delay)
    npzfile = np.load(ifile)
    
#    y = substract_bgr(npzfile['x'],npzfile['y'],4)
#    y[y<6]=0
    y = fit_data(p0, npzfile['x'],npzfile['y'])
    plt.plot(npzfile['x'],npzfile['y'], npzfile['x'], y)    
    
    npzfile.close()
    
plt.show()