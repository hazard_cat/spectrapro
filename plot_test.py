# -*- coding: utf-8 -*-
"""
Created on Sun Aug 17 20:02:07 2014

@author: Константин
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Aug 16 22:47:49 2014

@author: Константин
"""

import numpy as np

import glob

PATH='spectra/740/740_1/'

MASK = '*.npz'


files = glob.glob(glob.os.path.join(PATH, MASK))
for ifile in files[12:20]:
    F = np.load(ifile)
    delay = float(glob.os.path.basename(ifile).split('=')[1].replace('.npz','').replace('_','.'))
    plt.plot(F['x'], F['y'])
    #print ret
    F.close()

plt.show()