# -*- coding: utf-8 -*-
"""
Created on Wed Aug 13 23:03:10 2014

@author: Константин
"""

from libim7 import libim7
import numpy as np
import os
import logging

logger=logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s\n File: %(pathname)s , Line %(lineno)d')
ch.setFormatter(formatter)
logger.addHandler(ch)

IM7_FILE = '.im7'
IMX_FILE = '.imx'
PRF_FILE = '.prf'

IMAGE_FRAMES = 0
IMAGE_SPECTR = -3

class DataSource:
        
    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)




def load_image_file(fname=None, config=None, ignorescan=False):
    """
        Загружаем из файла всю ниобходимую информацию
    """
    root,ext = os.path.splitext(fname.lower())
    
    logger.info(ext)
    if ext in FILE_TYPES:
        return FILE_TYPES[ext](fname, config, ignorescan)


def read_image(fname,config,ignorescan):
    data = None
    try:
        buf, att = libim7.readim7(fname)
        att = att.as_dict()
        if not config is None:
            index = os.path.basename(fname).split('.')[0]
            i=0
            while (i<len(index)) and (not index[i] in '0123456789'):
                i=i+1
            index = float(index[i:])
            att[b'Scan-Value'] = config['delay-start']+index*config['delay-incr']
        
        # Test for scan-Value
        if ignorescan:
            att[b'Scan-Value']=0
            
        if not (b'Scan-Value' in att):
            return data
        
        if buf.ny>1:
            data = read_im7_2d(buf,att)
        else:
            data = read_im7_1d(buf,att)
    except:
        raise IOError('Error loading %s file.'%fname)
    return data
    

def read_im7_2d(buf,att):
    data = None
    try:
        x = buf.x
        y = buf.y
        frame = buf.get_frame(0)
        
        dy = int(y.size / 3)
        y1 = dy
        y2 = y1+dy
        
        _x0 = x.min()
        _x1 = x.max()
        _dx = float(_x1-_x0) / x.size
        
        x1 = float(_x0+_dx*x.size / 3.0)
        x2 = float(_x0+_dx*x.size * 2.0/ 3.0)

#        print(x1,x2)
        spectrum=frame[y1:y2,:].mean(axis=0)
        
        data = DataSource(x=x, y=y, frame=frame, y1=y1, y2=y2, x1=x1, x2=x2,\
                          xmin=_x0, xmax=_x1, ymin=y.min(), \
                          ymax=y.max(),spectrum=spectrum, \
                          delay=float(att[b'Scan-Value']), isImage=True)
        
    except:
        raise IOError('Error loading %s file.'%fname)
    
    return data

    
def read_im7_1d(buf,att):
    data = None
    try:
        x = buf.x
        frame = buf.get_frame(0)[0]
        
#        dy = int(y.size / 3)
#        y1 = dy
#        y2 = y1+dy
#        
        _x0 = x.min()
        _x1 = x.max()
        _dx = float(_x1-_x0) / x.size
        
        x1 = float(_x0+_dx*x.size / 3.0)
        x2 = float(_x0+_dx*x.size * 2.0/ 3.0)

        print(x1,x2)
#        spectrum=frame[y1:y2,:].mean(axis=0)
        
        data = DataSource(x=x, y=None, frame=frame, y1=None, y2=None, x1=x1, x2=x2,\
                          xmin=_x0, xmax=_x1, ymin=None, \
                          ymax=None,spectrum=frame, \
                          delay=float(att[b'Scan-Value']), isImage=False)
        
    except:
        raise IOError('Error loading %s file.'%fname)
    
    return data
    

def read_im7(fname):
    data = None
    try:
        buf,att = libim7.readim7(fname)
        att = att.as_dict()        
        if not (b'Scan-Value' in att):
            return data                
        #if buf.image_sub_type != IMAGE_FRAMES:
        #    raise Exception('Unsupported file content')
        
        x = buf.x
        y = buf.y
        frame = buf.get_frame(0)
        
        dy = int(y.size / 3)
        y1 = dy
        y2 = y1+dy
        
        _x0 = x.min()
        _x1 = x.max()
        _dx = float(_x1-_x0) / x.size
        
        x1 = float(_x0+_dx*x.size / 3.0)
        x2 = float(_x0+_dx*x.size * 2.0/ 3.0)

        print(x1,x2)
        spectrum=frame[y1:y2,:].mean(axis=0)
        
        data = DataSource(x=x, y=y, frame=frame, y1=y1, y2=y2, x1=x1, x2=x2,\
                          xmin=_x0, xmax=_x1, ymin=y.min(), \
                          ymax=y.max(),spectrum=spectrum, \
                          delay=float(att[b'Scan-Value']), isImage=True)
        
    except:
        raise IOError('Error loading %s file.'%fname)
    
    return data

def read_prf(fname):
    raise NotImplementedError()
    
    
def read_set_file(fname):
    config = {}
    
    f = open(fname,'rt')
    lines = f.readlines()
    print('Lines=%d'%len(lines))
    f.close()
    for line in lines:
        if 'ASD_DeviceSettings' in line:
            parts = line.split('=',1)[1].replace('"','').split('\\n')
            
            for item in parts:
                
                print(item)
                if 'Wavelength' in item:
                    print(item.strip().split()[2])
                    config['center-wavelength'] = float(item.strip().split()[2])
                elif 'Start' in item:
                    config['delay-start'] = float(item.strip().split()[1])
                elif 'Stop' in item:
                    config['delay-stop'] = float(item.strip().split()[1])
                elif 'Incr' in item:
                    config['delay-incr'] = float(item.strip().split()[1])

    return config

FILE_TYPES = {IM7_FILE:read_image,IMX_FILE:read_image,PRF_FILE:read_prf}
