# -*- coding: utf-8 -*-
"""
Created on Sat Aug 16 22:51:40 2014

@author: Константин
"""

import scipy.optimize as so
import numpy as np
Wo = 0.14259
lorentz = lambda x, x0, w : w**2/(w**2+(x-x0)**2)
gauss = lambda x, x0 : np.exp(-0.5*((x-x0)/(Wo))**2)
voigt = lambda x, n, x0, w: n*lorentz(x,x0,w)+(1.0-n)*gauss(x,x0)



def linear(p,x):
    a = p['a'].value
    b = p['b'].value
    return a*x+b

def approx(p,x):
    A=p['A'].value
    x0 = p['x0'].value
    w = p['w'].value
    return A*lorentz(x,x0,w)#+linear(p,x)

def approx_bgr(p,x,data):
    ret = approx(p,x)
    return ret-data

def approx_data_1_gauss(p,x,data=0):
    A0 = p['A0'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    return A0*gauss(x,x0,w0)-data


def approx_data_2_gauss(p,x,data=0):
    A0 = p['A0'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    A1 = p['A1'].value
    x1 = p['x1'].value
    w1 = p['w1'].value
    return A0*gauss(x,x0,w0)+A1*gauss(x,x1,w1)-data

def approx_data_3_gauss(p,x,data=0):
    A0 = p['A0'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    A1 = p['A1'].value
    x1 = p['x1'].value
    w1 = p['w1'].value
    A2 = p['A2'].value
    x2 = p['x2'].value
    w2 = p['w2'].value
    return A0*gauss(x,x0,w0)+A1*gauss(x,x1,w1)+A2*gauss(x,x2,w2)-data

def approx_data_1_lorentz(p,x,data=0):
    A0 = p['A0'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    return A0*lorentz(x,x0,w0)-data

def approx_data_2_lorentz(p,x,data=0):
    A0 = p['A0'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    A1 = p['A1'].value
    x1 = p['x1'].value
    w1 = p['w1'].value
    return A0*lorentz(x,x0,w0)+A1*lorentz(x,x1,w1)-data

def approx_data_3_lorentz(p,x,data=0):
    A0 = p['A0'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    A1 = p['A1'].value
    x1 = p['x1'].value
    w1 = p['w1'].value
    A2 = p['A2'].value
    x2 = p['x2'].value
    w2 = p['w2'].value
    return A0*lorentz(x,x0,w0)+A1*lorentz(x,x1,w1)+A2*lorentz(x,x2,w2)-data

def approx_data_1_voigt(p,x,data=0):
    A0 = p['A0'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    n0 = p['n0'].value
    return A0*voigt(x, n0, x0, w0) - data

def approx_data_2_voigt(p,x,data=0):
    A0 = p['A0'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    n0 = p['n0'].value
    A1 = p['A1'].value
    x1 = p['x1'].value
    w1 = p['w1'].value
    n1 = p['n1'].value
    return A0*voigt(x, n0, x0, w0)+A1*voigt(x, n1, x1, w1) - data

def approx_data_3_voigt(p,x,data=0):
    A0 = p['A0'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    n0 = p['n0'].value
    A1 = p['A1'].value
    x1 = p['x1'].value
    w1 = p['w1'].value
    n1 = p['n1'].value
    A2 = p['A2'].value
    x2 = p['x2'].value
    w2 = p['w2'].value
    n2 = p['n2'].value
    return A0*voigt(x, n0, x0, w0)+A1*voigt(x, n1, x1, w1)+A2*voigt(x, n2, x2, w2) - data


def approx_data_4_voigt(p,x,data=0):
    A0 = p['A0'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    n0 = p['n0'].value
    A1 = p['A1'].value
    x1 = p['x1'].value
    w1 = p['w1'].value
    n1 = p['n1'].value
    A2 = p['A2'].value
    x2 = p['x2'].value
    w2 = p['w2'].value
    n2 = p['n2'].value
    A3 = p['A3'].value
    x3 = p['x3'].value
    w3 = p['w3'].value
    n3 = p['n3'].value
    return A0*voigt(x, n0, x0, w0)+A1*voigt(x, n1, x1, w1)+\
            A2*voigt(x, n2, x2, w2)+A2*voigt(x, n3, x3, w3) - data



def report_3_voigt(F, params, ab, delay,hdr):
    if hdr == 1:
        header=""
        adds = ['Ibgr0','Ibgr1','Ibgr2', 'Delay','a','b']
        for name in params.keys()+adds:
            header+=name.center(12)
    #    F.write(header+"\n")
        F.write(header+"\n")
        hdr = 0

    line = ""
    for name in params.keys():
        line+="%12.4f"%params[name].value

    adds1 = []
    adds1.append(ab[0]*params['x0'].value+ab[1])
    adds1.append(ab[0]*params['x1'].value+ab[1])
    adds1.append(ab[0]*params['x2'].value+ab[1])
    adds1.append(delay)
    adds1.append(ab[0])
    adds1.append(ab[1])

    for i in range(6):
        line+="%12.4f"%(adds1[i])

    F.write(line+"\n")
    return hdr


def report_4_voigt(F, params, ab, delay,hdr):
    if hdr == 1:
        header=""
        adds = ['Ibgr0','Ibgr1','Ibgr2', 'Ibgr4', 'Delay','a','b']
        for name in params.keys()+adds:
            header+=name.center(12)
    #    F.write(header+"\n")
        F.write(header+"\n")
        hdr = 0

    line = ""
    for name in params.keys():
        line+="%12.4f"%params[name].value

    adds1 = []
    adds1.append(ab[0]*params['x0'].value+ab[1])
    adds1.append(ab[0]*params['x1'].value+ab[1])
    adds1.append(ab[0]*params['x2'].value+ab[1])
    adds1.append(ab[0]*params['x3'].value+ab[1])
    adds1.append(delay)
    adds1.append(ab[0])
    adds1.append(ab[1])

    for i in range(7):
        line+="%12.4f"%(adds1[i])

    F.write(line+"\n")
    return hdr
