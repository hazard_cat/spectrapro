# -*- coding: utf-8 -*-
"""
Created on Sun Aug 10 12:41:03 2014

@author: Константин
"""

import numpy as np
import scipy.optimize as no

AVAILABLE_FITS = ['Lorentz','Gauss','Voigt']

def gauss_fun(xdata, *params):
    x0,w = xdata[2], xdata[3]
    x = params[0]
    g = np.exp(-np.log(2)*((x-x0)/w)**2)
    return g

def lorentz_fun(xdata, *params):
    x0,w = xdata[2], xdata[3]
    x = params[0]
    l = 1.0 / (1.0 + ((x-x0)/w)**2)
    return l

def voigt_fun(xdata, *params):
    etha,A=xdata[0],xdata[1]
    bgr = 0
    lor = lorentz_fun(xdata, *params)
    gas = gauss_fun(xdata, *params)
    y = params[1]
    return np.sum(np.abs(((y-bgr)-A*(etha*lor+(1-etha)*gas))))

def voigt_fun1(xdata, *params):
    etha,A=xdata[0],xdata[1]
    bgr = 0
    lor = lorentz_fun(xdata, *params)
    gas = gauss_fun(xdata, *params)
    y = params[1]
    return A*(etha*lor+(1-etha)*gas)+bgr

FIT_FUN = (lorentz_fun, gauss_fun, voigt_fun)

def fitspectra(xdata, spectra, x_range, fit_index, removebgr):
    idx = (xdata>=x_range[0])&(xdata<=x_range[1])
    x = xdata[idx]
    y = spectra[idx]
    fun = voigt_fun

    xdata0 = np.array([0.3, 10, 337.0, 1])
#    params = (x,y)
    res = no.fmin_l_bfgs_b(fun, xdata0, args=(x,y-50), bounds=[(0,1.0),(0, None),(0,None), (0,None)], approx_grad=True)
    y1 = voigt_fun1(res[0],xdata,y)
    return y1+50


