# -*- coding: utf-8 -*-
"""
Created on Sat Aug 16 22:35:23 2014

@author: Константин
"""

import pylab as plt

import numpy as np
from lmfit import minimize, Parameters, Parameter, report_fit, fit_report
#папки 337 и 391, 771_1 и 777_3 функция файла 632_1200.
#вся 740 и 777_2 и 777_4  функция файла 632_1200 632_600.
gauss = lambda x, x0, w0: np.exp(-0.5*((x-x0)/(w0))**2)

def appr(p,x,data=0):
    A = p['A'].value
    x0 = p['x0'].value
    w0 = p['w0'].value
    return A*gauss(x,x0,w0)-data




x,y= np.loadtxt('he_632_1200_100mkm.txt', unpack=True)

params = Parameters()
params.add('A',value=100,min=0,max=1000)
params.add('x0',value=633, min=632, max=634)
params.add('w0',value=0.14, min=0, max=10.0)

ret = minimize(appr, params, args=(x,y))
yy = appr(ret.params, x)
report_fit(ret.params)
plt.plot(x,y,x,yy/ret.params['A'].value)
#plt.plot(pack['x'], pack['y'])
pack.close()
plt.show()