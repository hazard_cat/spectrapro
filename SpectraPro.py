# -*- coding: utf-8 -*-
"""
Created on Wed Jul 23 11:29:47 2014

@author: Администратор

"""
from PyQt4 import QtCore, QtGui
from interface.InterfaceFrame import Ui_MainWindow
from interface.DataLoadDialog import DataLoadDialog
import numpy as np
from libim7 import libim7
from datetime import datetime
from utils.math import AVAILABLE_FITS, fitspectra
import logging 
import utils.datasource as datasource
import os


#_fromUtf8 = QtCore.QString.fromUtf8

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s\n File: %(pathname)s , Line %(lineno)d')
ch.setFormatter(formatter)
logger.addHandler(ch)

class ImageWindow(QtGui.QMainWindow, Ui_MainWindow):
    
    
    def __init__(self):
        self.data_sources = []
        
        QtGui.QMainWindow.__init__(self)
        self.setupUi(self)
        logger.info('Constructing interface')
        self.image_plot.figure.subplots_adjust(left=0.10, bottom=0.16)
        self.image_plot.axes.set_xlabel('$\lambda, (\mu m)$')
        self.image_plot.axes.set_ylabel('Row (pix.)')
        self.image_plot.axes.grid(True, color='white')
        self.matr_plot = self.image_plot.axes.imshow(np.random.rand(10,10),\
                        origin='lower', aspect='auto')
        self.y1line = self.image_plot.axes.axhline(0, color='r', linewidth=2)
        self.y2line = self.image_plot.axes.axhline(0, color='m', linewidth=2)
        
        self.curve_plot.figure.subplots_adjust(left=0.10, bottom=0.16)
        self.curve_plot.axes.set_xlabel('$\lambda, (\mu m)$')
        self.curve_plot.axes.set_ylabel('Intensity (a.u.)')
        
        self.spectrum_plot = self.curve_plot.axes.plot([],[])
        self.curve_plot.axes.grid(True)
        self.x1line = self.curve_plot.axes.axvline(0, color='r', linewidth=2)
        self.x2line = self.curve_plot.axes.axvline(0, color='m', linewidth=2)
        self.cmbApproxType.addItems(AVAILABLE_FITS)
        

    
        
    def load_image(self):
        setfile, datafiles, scanvalue, ok = DataLoadDialog.getFiles(self)
        
        if ok:
            logger.info('Entering load image section')
            self.files_list.clear()

            del self.data_sources[:]        
            self.data_sources = []
            cfg=None
            if setfile!="":
                cfg = datasource.read_set_file(setfile)
                print(cfg)
            
                
            if len(datafiles) != 0: 
                for item in datafiles:
                    logger.info('Loading %s file'%item)
                    
                    data = datasource.load_image_file(str(item), cfg, not scanvalue)
                    if not (data is None):
                        self.files_list.addItem(str(item))
                        self.data_sources.append(data)
                    else:
                        logger.error('Problem loading %s file, skipping it.'%item)
    #                    raise Exception('Problem loading %s file'%item)
                logger.info('Loading process finished')
                
    

    def redraw_image(self,data):
        if data.isImage:
            extent = (data.xmin, data.xmax, data.ymin, data.ymax)        
            self.matr_plot.set_data(data.frame)
            self.matr_plot.autoscale()
            self.matr_plot.set_extent(extent)
            self.image_plot.draw()
    
    def redraw_axis1(self, data):
        """
        перерисовывает обновленную нформацию на графике
        """
        if data.isImage:
            self.y1line.set_ydata(data.y1)
            self.y2line.set_ydata(data.y2)        
            self.image_plot.draw()
        
    def redraw_axis2(self, data):

        self.spectrum_plot[0].set_xdata(data.x)
        self.spectrum_plot[0].set_ydata(data.spectrum)
        
        
        self.curve_plot.axes.relim()
        self.curve_plot.axes.autoscale_view()
        self.curve_plot.axes.set_xlim((data.xmin,data.xmax))
        
        self.x1line.set_xdata(data.x1)
        self.x2line.set_xdata(data.x2)
        self.curve_plot.draw()
        
        
    def update_spectrum(self, data, index, calc_spectrum=True):
        if calc_spectrum:
            data.spectrum=data.frame[data.y1:data.y2,:].mean(axis=0)
        self.data_sources[index] = data    
    
    def set_init_spin_values(self, data):
        
        self.doubleSpinBoxX1.setMaximum(data.xmax)
        self.doubleSpinBoxX1.setMinimum(data.xmin)
        self.doubleSpinBoxX2.setMaximum(data.xmax)
        self.doubleSpinBoxX2.setMinimum(data.xmin)
        self.doubleSpinBoxX1.setValue(data.x1)
        self.doubleSpinBoxX2.setValue(data.x2)
        
        if data.isImage:
            self.spinBoxY1.setMaximum(data.ymax)
            self.spinBoxY1.setMinimum(data.ymin)
            self.spinBoxY2.setMaximum(data.ymax)
            self.spinBoxY2.setMinimum(data.ymin)
            self.spinBoxY1.setValue(data.y1)
            self.spinBoxY2.setValue(data.y2)
        
   
    
    def file_list_item_changed(self, item):
        index = self.files_list.currentIndex().row()
        data = self.data_sources[index]
        logger.info('Selected index = %d'%index)
        
        self.set_init_spin_values(data)
        self.set_preferences(data)
        self.redraw_image(data)
        self.redraw_axis1(data)
        self.redraw_axis2(data)




    
    def set_preferences(self, data):
        self.leDelay.setText(str(data.delay))
        return
    
           
            
    def spinXValueChanged(self, value):      
        
        
        index = self.files_list.currentIndex().row()
        data = self.data_sources[index]
        data.x1 = self.doubleSpinBoxX1.value()
        data.x2 = self.doubleSpinBoxX2.value()
        self.update_spectrum(data, index, False)
        self.redraw_axis2(data)
    
            
    def fitSpectra(self):
        fit_index = self.cmbApproxType.currentIndex() #fittype
        self._appLog('Fit index = %d'%fit_index)
        
        spectra = self.spectraplt.get_ydata()
        
        xdata = self.spectraplt.get_xdata()
        
        x_range = self.x1line.get_xdata(),self.x2line.get_xdata()
        
        removebgr = self.chkRemoveBgr.isChecked()
        
        
        result = fitspectra(xdata, spectra, x_range, fit_index, removebgr)
        self.spectraplt.set_ydata([spectra, result])
        self.spectraplt.set_xdata([xdata, xdata])
        self.curve_plot.axes.relim()
        self.curve_plot.axes.autoscale_view()

        self.curve_plot.draw()  
        print(result)
        pass
    
    def changeYCursors(self):
        indices = self.files_list.selectedIndexes()
        
        for index in indices:
            data = self.data_sources[index.row()]
            data.y1 = self.spinBoxY1.value()
            data.y2 = self.spinBoxY2.value()
            self.update_spectrum(data, index.row())
        data=self.data_sources[indices[0].row()]
        self.redraw_axis1(data)
        self.redraw_axis2(data)
    
    def setXAxis(self):
        selected = self.files_list.currentIndex().row()
        tmp = self.data_sources[selected]
        indices = self.files_list.selectedIndexes()
        
        for index in indices:
            data = self.data_sources[index.row()]
            data.x = tmp.x.copy()
            data.x1 = tmp.x1
            data.x2 = tmp.x2
            data.xmax = tmp.xmax
            data.xmin = tmp.xmin
            self.update_spectrum(data, index.row(), False)
        self.redraw_axis1(data)
        self.redraw_axis2(data)
        
    def changeXCursors(self):
        indices = self.files_list.selectedIndexes()
    
        
        for index in indices:
            #index = self.files_list.currentIndex().row()
            data = self.data_sources[index.row()]
            data.x1 = self.doubleSpinBoxX1.value()
            data.x2 = self.doubleSpinBoxX2.value()
            self.update_spectrum(data, indices[0].row(), False)
#        self.redraw_axis1(data)
        self.redraw_axis2(self.data_sources[indices[0].row()])
    
    def export_file(self, index, fname):
        data = self.data_sources[index]
        if data.x1<data.x2:
            idx = (data.x>data.x1) & (data.x<data.x2)
        else:
            idx = (data.x>data.x2) & (data.x<data.x1)
        
        name,ext = os.path.splitext(fname)
        ext=ext.lower()
        name=name.lower()
        delay = ("_delay=%06.2f"%data.delay).replace(".","_")
        sfile=name+delay+ext
        if '.npz' in ext:
            np.savez(sfile,x = data.x[idx], y=data.spectrum[idx])
        elif ('.txt' in ext) or ('.dat' in ext):
            np.savetxt(sfile,list(zip(data.x[idx], data.spectrum[idx])), delimiter='\t',
                       fmt='%10.4f')
    
    def export_spectrum(self):
        indices = self.files_list.selectedIndexes()
        sfile = str(QtGui.QFileDialog.getSaveFileName(
                         self,
                         "Select file to save",
                         "",
                         "Text Files (*.txt *.dat );;Numpy files (*.npz)"))
        
        
        if sfile is not None:
            for index in indices:
                self.export_file(index.row(), sfile)
                        
        
if __name__ == "__main__":
    import sys
   
    app = QtGui.QApplication(sys.argv)
#    dd = DataLoadDialog()
#    dd.show()
    
    
    MainWindow = ImageWindow()
    MainWindow.show()
    sys.exit(app.exec_())
        
        