# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Test.ui'
#
# Created: Wed Jul 23 14:45:57 2014
#      by: PyQt4 UI code generator 4.9.6
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1110, 777)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.splitter = QtGui.QSplitter(self.centralwidget)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.widget = QtGui.QWidget(self.splitter)
        self.widget.setObjectName(_fromUtf8("widget"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.widget)
        self.verticalLayout_4.setMargin(0)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_2 = QtGui.QLabel(self.widget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout.addWidget(self.label_2)
        self.horizontalSliderY1 = QtGui.QSlider(self.widget)
        self.horizontalSliderY1.setMaximum(1024)
        self.horizontalSliderY1.setProperty("value", 400)
        self.horizontalSliderY1.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSliderY1.setObjectName(_fromUtf8("horizontalSliderY1"))
        self.horizontalLayout.addWidget(self.horizontalSliderY1)
        self.spinBoxY1 = QtGui.QSpinBox(self.widget)
        self.spinBoxY1.setMaximum(1024)
        self.spinBoxY1.setProperty("value", 400)
        self.spinBoxY1.setObjectName(_fromUtf8("spinBoxY1"))
        self.horizontalLayout.addWidget(self.spinBoxY1)
        self.label_3 = QtGui.QLabel(self.widget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout.addWidget(self.label_3)
        self.horizontalSliderY2 = QtGui.QSlider(self.widget)
        self.horizontalSliderY2.setMaximum(1024)
        self.horizontalSliderY2.setProperty("value", 600)
        self.horizontalSliderY2.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSliderY2.setObjectName(_fromUtf8("horizontalSliderY2"))
        self.horizontalLayout.addWidget(self.horizontalSliderY2)
        self.spinBoxY2 = QtGui.QSpinBox(self.widget)
        self.spinBoxY2.setMaximum(1024)
        self.spinBoxY2.setProperty("value", 600)
        self.spinBoxY2.setObjectName(_fromUtf8("spinBoxY2"))
        self.horizontalLayout.addWidget(self.spinBoxY2)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout_4.addLayout(self.horizontalLayout)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.image_plot = MatplotlibWidget(self.widget)
        self.image_plot.setObjectName(_fromUtf8("image_plot"))
        self.verticalLayout_3.addWidget(self.image_plot)
        self.curve_plot = MatplotlibWidget(self.widget)
        self.curve_plot.setObjectName(_fromUtf8("curve_plot"))
        self.verticalLayout_3.addWidget(self.curve_plot)
        self.verticalLayout_4.addLayout(self.verticalLayout_3)
        self.widget1 = QtGui.QWidget(self.splitter)
        self.widget1.setObjectName(_fromUtf8("widget1"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.widget1)
        self.verticalLayout_2.setMargin(0)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.group_properties = QtGui.QGroupBox(self.widget1)
        self.group_properties.setObjectName(_fromUtf8("group_properties"))
        self.verticalLayout = QtGui.QVBoxLayout(self.group_properties)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label = QtGui.QLabel(self.group_properties)
        self.label.setObjectName(_fromUtf8("label"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label)
        self.lineEdit = QtGui.QLineEdit(self.group_properties)
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineEdit)
        self.verticalLayout.addLayout(self.formLayout)
        self.verticalLayout_2.addWidget(self.group_properties)
        self.files_list = QtGui.QListWidget(self.widget1)
        self.files_list.setObjectName(_fromUtf8("files_list"))
        self.verticalLayout_2.addWidget(self.files_list)
        self.horizontalLayout_2.addWidget(self.splitter)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1110, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menu = QtGui.QMenu(self.menubar)
        self.menu.setObjectName(_fromUtf8("menu"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.action = QtGui.QAction(MainWindow)
        self.action.setObjectName(_fromUtf8("action"))
        self.actionOpen = QtGui.QAction(MainWindow)
        self.actionOpen.setObjectName(_fromUtf8("actionOpen"))
        self.actionPreferences = QtGui.QAction(MainWindow)
        self.actionPreferences.setObjectName(_fromUtf8("actionPreferences"))
        self.menu.addAction(self.actionOpen)
        self.menubar.addAction(self.menu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QObject.connect(self.actionOpen, QtCore.SIGNAL(_fromUtf8("activated()")), MainWindow.load_image)
        QtCore.QObject.connect(self.files_list, QtCore.SIGNAL(_fromUtf8("currentItemChanged(QListWidgetItem*,QListWidgetItem*)")), MainWindow.file_list_item_changed)
        QtCore.QObject.connect(self.spinBoxY1, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), MainWindow.spinValueChanged)
        QtCore.QObject.connect(self.spinBoxY2, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), MainWindow.spinValueChanged)
        QtCore.QObject.connect(self.horizontalSliderY1, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), self.spinBoxY1.setValue)
        QtCore.QObject.connect(self.horizontalSliderY2, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), self.spinBoxY2.setValue)
        QtCore.QObject.connect(self.spinBoxY1, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), self.horizontalSliderY1.setValue)
        QtCore.QObject.connect(self.spinBoxY2, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), self.horizontalSliderY2.setValue)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.label_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">Y1 =</span></p></body></html>", None))
        self.label_3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff00ff;\">Y2 =</span></p></body></html>", None))
        self.group_properties.setTitle(_translate("MainWindow", "Свойства", None))
        self.label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" text-decoration: underline;\">Имя файла:</span></p></body></html>", None))
        self.menu.setTitle(_translate("MainWindow", "Файл", None))
        self.action.setText(_translate("MainWindow", "Открыть...", None))
        self.actionOpen.setText(_translate("MainWindow", "Открыть...", None))
        self.actionOpen.setShortcut(_translate("MainWindow", "Ctrl+O", None))
        self.actionPreferences.setText(_translate("MainWindow", "Настройка...", None))

from matplotlibwidget import MatplotlibWidget
