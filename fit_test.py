# -*- coding: utf-8 -*-
"""
Created on Sat Aug 16 22:47:49 2014

@author: Константин
"""

import numpy as np
import pylab as plt
import glob
from lmfit import minimize, Parameters, Parameter, report_fit, fit_report
from utils.fitpack import approx_bgr,linear,approx,approx_data_1_gauss,\
                        approx_data_1_lorentz, approx_data_2_gauss,\
                        approx_data_2_lorentz, approx_data_3_gauss,\
                        approx_data_3_lorentz, approx_data_3_voigt,\
                        approx_data_2_voigt, approx_data_1_voigt,\
                        report_3_voigt, approx_data_4_voigt,\
                        report_4_voigt
import gvar


PATH='spectra/740/740_2/'
OUTFILE='spectr_740_2_fit.txt'
MASK = '*.npz'


files = glob.glob(glob.os.path.join(PATH, MASK))
params = Parameters()
params.add('A0', value=1000.0)
params.add('x0',value=749.5, min=749, max=750, vary=True)
params.add('w0',value=0.20, min=0.01, max=15.4)
params.add('n0',value=0.5, min=0, max=1.0)

params.add('A1', value=1000.0)
params.add('x1',value=746.2, min=746, max=746.8, vary=True)
params.add('w1',value=0.20, min=0.01, max=15.4)
params.add('n1',value=0.5, min=0, max=1.0)

params.add('A2', value=1000.0)
params.add('x2',value=744.2, min=743.8, max=744.8, vary=True)
params.add('w2',value=0.20, min=0.01, max=15.4)
params.add('n2',value=0.5, min=0, max=1.0)

#params.add('A3', value=1000.0)
#params.add('x3',value=753.4, min=752.7, max=753.7, vary=True)
#params.add('w3',value=0.20, min=0.01, max=15.4)
#params.add('n3',value=0.5, min=0, max=1.0)

header = 1
with open(OUTFILE, 'wt') as FILE:
    x0=736
    x1 = 757
    for ifile in files[:15]:
#    for ifile in files[:12]:
        params['n0'].value = 0.5
        params['n1'].value = 0.5
        params['n2'].value = 0.5
#        params['n3'].value = 0.5

        F = np.load(ifile)
        delay = float(glob.os.path.basename(ifile).split('=')[1].replace('.npz','').replace('_','.'))

        idx = (F['x']>x0)&(F['x']<x1)
        xbgr = F['x'][~idx]
        ybgr = F['y'][~idx]
        A = np.column_stack((xbgr, np.ones(xbgr.size),))
        ab = np.linalg.lstsq(A, ybgr)[0]
        linebgr = ab[0]*F['x']+ab[1]

        xfit = F['x'][idx]
        yfit = F['y'][idx] - linebgr[idx]

        ret = minimize(approx_data_3_voigt, params,args=(xfit, yfit))
        header = report_3_voigt(FILE, ret.params, ab, delay,header)
#        FILE.write(ifile+'\n')
#        FILE.write(fit_report(ret.params,show_correl=False)+'\n')

        spectrum = approx_data_3_voigt(ret.params, xfit)#F['y'] - linear( ret.params,F['x'])
        #spectrum /= spectrum.max()
        #spectrum[spectrum<0.05] = 0
        plt.plot(xfit,spectrum, xfit, yfit)
        #print ret
        F.close()

plt.show()